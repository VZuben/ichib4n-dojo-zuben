# <strong>ichib4n-dojo - Zuben

## Introdução

Autor: Pedro Henrique Figueiredo Von Zuben
<br>
Descrição: Repositório remoto feito para o trabalho de conclusão do treinamento de wordpress da EJCM de 2022.2
<br>
<hr>
    
## 000webhostapp ##

Link do site hospedado em "000webhostapp": https://ichib4n-dojo.000webhostapp.com
